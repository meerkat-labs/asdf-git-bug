# Contributing

Testing Locally:

```shell
asdf plugin test <plugin-name> <plugin-url> [--asdf-tool-version <version>] [--asdf-plugin-gitref <git-ref>] [test-command*]

#
asdf plugin test git-bug https://github.com/meerkatlabs/asdf-git-bug.git "git-bug --version"
```

Tests are automatically run in GitHub Actions on push and PR.
