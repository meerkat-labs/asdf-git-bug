<div align="center">

# asdf-git-bug ![Build](https://github.com/meerkatlabs/asdf-git-bug/workflows/Build/badge.svg) ![Lint](https://github.com/meerkatlabs/asdf-git-bug/workflows/Lint/badge.svg)

[git-bug](https://github.com/MichaelMure/git-bug) plugin for the [asdf version manager](https://asdf-vm.com).

</div>

# Contents

- [Dependencies](#dependencies)
- [Install](#install)
- [Why?](#why)
- [Contributing](#contributing)
- [License](#license)

# Dependencies

- `bash`, `curl`, `tar`: generic POSIX utilities.
- `SOME_ENV_VAR`: set this environment variable in your shell config to load the correct version of tool x.

# Install

Plugin:

```shell
asdf plugin add git-bug
# or
asdf plugin add https://github.com/meerkatlabs/asdf-git-bug.git
```

git-bug:

```shell
# Show all installable versions
asdf list-all git-bug

# Install specific version
asdf install git-bug latest

# Set a version globally (on your ~/.tool-versions file)
asdf global git-bug latest

# Now git-bug commands are available
git-bug --version
```

Check [asdf](https://github.com/asdf-vm/asdf) readme for more instructions on how to
install & manage versions.

# Contributing

Contributions of any kind welcome! See the [contributing guide](contributing.md).

[Thanks goes to these contributors](https://github.com/meerkatlabs/asdf-git-bug/graphs/contributors)!

# License

See [LICENSE](LICENSE) © [Robert Robinson](https://github.com/meerkatlabs/)
